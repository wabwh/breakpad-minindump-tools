
1. 将dumo_syms.exe pdb2sym.bat uploadSym.bat拷贝到pdb文件目录下；
2. 执行pdb2sym.bat文件  ，利用当前文件夹下的.pdb文件生成对应的.sym文件
3. 执行uploadSym.bat 把当前目录下的.sym文件上传到服务器上。
默认上传服务器地址为打包机器的地址 172.16.82.6:1127
可通过传参方式修改服务器地址
  uploadSym.bat 127.0.0.1:1127
  uploadSym.bat http://simple-breakpad.vnnox.com
