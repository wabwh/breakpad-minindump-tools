# breakpad minindump tools

#### 介绍
解析dump的工具

#### 软件架构
软件架构说明


#### 安装教程

1.  dump_syms.exe 用来将符号文件（PDB）解析成sym格式的符号文件
        dump_syms.exe A.pdb >A.sym
2.  minidump_stackwalk.exe 用来解析dump，用法
	minidump_stackwalk.exe  XX.dmp A.sym
	
	minidump_stackwalk.exe [options] <minidump-file> [symbol-path ...]
	Output a stack trace for the provided minidump
	Options:
		  -m         Output in machine-readable format
		  -s         Output stack contents
3.  simple-breakpad-server
安装：npm install -g simple-breakpad-server
运行： npm install
		npm run-script serve
服务器地址： localhost:1127
https://github.com/acrisci/simple-breakpad-server
https://github.com/electron/electron/blob/master/docs/api/crash-reporter.md

#### 文件上传
1.  dmp文件上传
使用curl 上传

curl -F upload_file_minidump=@4518a0a4-9404-4cc1-a250-9cda41eccc97.dmp -F ver="0.0.1"  -F prod=cef localhost:1127/crashreports

2.  符号文件上传：
 curl -F symfile=@effectdisplay.sym localhost:1127/symfiles

#### 使用说明

1. 应用minidump_stackwalk分析dmp文件，以app.exe程序名称为例
	用dump_syms.exe 生成symbol文件（名称为：app.sym）
    命令：dump_syms app.pdb
2. 将生成的dmp文件Copy到本目录（exe和pdb文件目录）；
	取sym文件的第一行：一般形式是：
	MODULE windows x86_64 EB4B350D74B8461AA79E7D1F82A0A2C01 
	在当前目录下，新建目录: “symbols/app.pbd/”;
	在目录“symbols/app.pbd/”下再新建目录：“EB4B350D74B8461AA79E7D1F82A0A2C01”
	将app.sym Copy到上述目录中；
	
3. 运行：minidump_stackwalk.exe -s aaa.dmp ./symbols > app.txt
几点说明：
	“app”要改为自己的程序名称；
	Sym文件一定要与程序名称相同；
	Symbols目录下“app.pbd”一定要包含“.pdb”字串


#### 安装问题解决
1.  npm 清理缓存
npm cache clean -f
2.  npm ERR! Failed at the XXX@X.X.X install script
npm install XXX@X.X.X --ignore-scripts
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
